MEDIBOT TK-4500 : MODULE SCANNER

/////+++++\\\\\
CLIC DROIT sur le dossier SCANs.rar -> Extraire ici -> MOT DE PASSE : README
/////+++++\\\\\

INSTRUCTIONS :

Une fois la routine de scans effectu�e sur le patient, le module g�n�re automatiquement cinq fichiers.
Ces derniers correspondent aux l�sions majeures du patient, qui doivent �tre trait�es avec la plus grande vigilance.
Les donn�es clef des scans doivent �tre manuellement rentr�es sous forme de connexions filaires sur le module par un op�rateur.
Pour une utilisation optimale, imprimez tous les scans.


F.A.Q (Frequently Asked Questions)

1. Un de mes scanners semble d�coup� en morceaux carr�s
2. Plusieurs fichiers repr�sentent le m�me scanner
3. Dois-je m'arreter l� ?
4. Le fichier audio de s�lection de scanner est corrompu
5. Un fichier scanner ne peut pas �tre ouvert du tout
6. L'ordinateur est lent
7. Un fichier d'archive est g�n�r� mais prot�g� par mot de passe
8. Y a-t-il un chat � l'int�rieur de la boite ?


---


1. Un de mes scanners semble d�coup� en morceaux carr�s

Une telle anomalie peut appara�tre lorsque le patient a des ant�c�dents d'op�rations ant�rieures.
Un tel fichier disposera de l'extension de fichier.xcf.
Ouvrez le avec le logiciel "GIMP".
Assurez-vous que les calques sont bien tous affich�s (symboles "oeil" dans la rubrique calque).
Si ce n'est pas le cas, cliquez sur la case o� devrait se trouver l'oeil.
Utilisez l'outil de d�placement (raccourci "M") pour r�organiser les carr�s.


2. Plusieurs fichiers repr�sentent presque le m�me scanner

Il existe une probabilit� d'environ 0.1% que le scanner retourne � l'op�rateur une incertitude non d�codable.
Le cas �ch�ant, plusieurs fichiers de scanners repr�sentant les diff�rentes possibilit�s sont g�n�r�s.
Un fichier audio de s�l�ction permet � l'op�rateur de s�lectionner le fichier le plus probable.


3. Dois-je m'arreter l� ?

Vraiment ? Si vous �tes bloqu� c'est qu'il faut continuer de lire ce README.txt


4. Le fichier audio de s�lection de scanner est corrompu

Un bug tr�s rare peut subvenir lors de la g�n�ration du fichier audio (voir rubrique pr�c�dente).
Le fichier audio doit alors �tre retrait� avec le logiciel "Audacity".
Ouvrez le logiciel et glissez-d�posez le fichier audio dans la fen�tre principale.
Plusieurs traitements sont envisageables en fonction du bug identifi� du fichier (le plus souvent, un seul traitement suffit) :

- Amplification sonore : "Effets" -> "Amplification", puis choix de l'amplification en decibels (unit� de mesure du volume sonore)
L'amplification la plus courante

- Inversion de la bande : "Effets" -> "Inversion sens"

- Coupure sonore : s�lectionnez la partie de la bande son que vous souhaitez tronquer et appuyez sur le bouton "Delete" sur le clavier


5. Un fichier scanner ne peut pas �tre ouvert du tout

V�rifiez les extensions des fichiers et changez les au besoin.
Si les extensions ne sont pas visibles, allez dans le menu "affichage" et cochez "extensions de noms de fichiers".


6. L'ordinateur est lent

D�sol� si c'est le cas, nous ne pouvons rien. Esperons que vous r�ussirez quand m�me : ne laissez pas les ordinateurs avoir le dessus sur vous. 


7. Un fichier d'archive est g�n�r� mais prot�g� par mot de passe

[trouver le mot de passe]
Pour le trouver, un syst�me d'encodage a �t� mis en place au format SVG.
Ouvrez le avec le logiciel "Inkscape".
Zoomez au besoin (raccourci F3).
Reliez les points NOIRS avec l'outil de trac� de courbes (raccourci Maj + F6).


8. Y a-t-il un chat � l'int�rieur de la boite ?

Peut-�tre bien que oui, peut-�tre bien que non.
En fonction du temps que vous mettrez pour terminer ce jeu nous vous en dirons plus alors d�p�chez-vous plut�t que de lire ce README.txt maitenant que vous �tes arriv� au bout !